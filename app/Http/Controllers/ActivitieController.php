<?php

namespace App\Http\Controllers;

use App\Time;
use App\AO\TimeAO;
use App\AO\ActivitieAO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ActivitieRequest;

class ActivitieController extends Controller
{
    function __construct(ActivitieAO $activitie, TimeAO $time){
        $this->activitie = $activitie;
        $this->time = $time;
    }

    public function index()
    {
        $idUser = auth()->user()->id;
        $activities = $this->activitie->getActivities($idUser);
        return view('activitie.index', compact('activities'));
    }

    public function store(ActivitieRequest $request)
    {
        $validated = $request->validated();

        if ($request->hour > 8) {
            return 1;
        }

        $idUser = auth()->user()->id;
        $idActivitie = $this->activitie->add($request->name, $idUser);
        $this->time->add($request->date, $request->hour, $idActivitie);

        return $request->name;
    }

    public function show($id)
    {
        $activitie = $this->activitie->getActivitie($id);
        return view('activitie.show', compact('activitie'));
    }

    public function update(Request $request, $idActivitie)
    {
        if ($this->validateHour($request->hour, $idActivitie)) {
            return 1;
        }

        $this->time->add($request->date, $request->hour, $idActivitie);

        return 0;

    }

    public function validateHour($hour, $id){
        $hourNow = $this->getHour($id);
        $totalHour = $hour + $hourNow;

        if ($totalHour > 8) {
            return true;
        } else {
            return false;
        }

    }

    public function getHour($id){
        $activitie = $this->activitie->getActivitie($id);

        $count = 0;
        foreach ($activitie->times as $time) {
           $count += $time->hour;
        }

        return $count;
    }

}
