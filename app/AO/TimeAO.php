<?php

namespace App\AO;

use App\Time;

class TimeAO {

    function __construct(Time $time){
        $this->time = $time;
    }

    function add($date, $hour, $id){
        $this->time::insert([
            'date'=> $date,
            'hour'=> $hour,
            'activitie_id'=> $id,
        ]);
    }
}
