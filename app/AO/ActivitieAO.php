<?php

namespace App\AO;

use App\Activitie;

class ActivitieAO {

    function __construct(Activitie $activitie) {
        $this->activitie = $activitie;
    }

    public function add($nameActivitie, $idUSer){
        return $this->activitie::insertGetId(
            [
                'name' => $nameActivitie,
                'user_id' => $idUSer
            ]);
    }

    public function getActivities($idUser){
        return $this->activitie::where('user_id', $idUser)->get();
    }

    public function getActivitie($id){
        return $this->activitie::with('times')->where('id', $id)->first();
    }
}
