@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <p>Name: {{$activitie->name}}</p>
                    <p>Times</p>
                    @foreach ($activitie->times as $time)
                        <p>Date: {{ $time->date }} - hour: {{ $time->hour }}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
