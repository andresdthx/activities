@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    My activities
                </div>
                <div class="card-body">

                    {{-- <index-company
                        :companies="{{$comps}}"
                    ></index-company> --}}

                    <index-activitie
                        :activities="{{ $activities }}"
                    ></index-activitie>


                    {{-- <div class="center-block">{{$cop}}</div> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
