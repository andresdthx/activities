@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Arrays
                </div>
                <div class="card-body">
                    <javascript></javascript>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
